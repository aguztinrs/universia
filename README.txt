** IMPORTANTE **

* Para subir los archivos a un servidor de producción usar los archivos contenidos en la carpeta "prod".
  Los siguientes archivos han sido minificados para su puesta en producción:

    - dev/styles.css
    - dev/yui-reset.css

* Para editar los archivos del sitio web usar los archivos contenidos en la carpeta "dev".
